const React = require('react')
const Feature = require('./Feature.jsx')

module.exports = ({
  features = [],
  featureName = '',
  featureText = '',
  date = '(unknown)',
  currentText = '',
  canEdit,
  canSave,
  canCreateNew,
  onFeatureSelected,
  onBackPressed,
  onForwardPressed,
  onNameChanged,
  onMarkdownChanged,
  onMarkdownSave,
  onMarkdownSaveNew
}) => {
  return <div>
    <select
      value={featureName}
      onChange={(e) => {
        onFeatureSelected(e.target.value)
      }}>
      {features.map(feature => (
        <option>{feature}</option>
      ))}
    </select>
    <button onClick={onBackPressed}>Back</button>
    <button onClick={onForwardPressed}>Forward</button>
    <hr />
    <span>On {date}:</span>
    <Feature
      featureName={featureName}
      featureText={featureText}
      currentText={currentText}
      canEdit={canEdit}
      canSave={canSave}
      canCreateNew={canCreateNew}
      onNameChanged={onNameChanged}
      onMarkdownChanged={onMarkdownChanged}
      onMarkdownSave={onMarkdownSave}
      onMarkdownSaveNew={onMarkdownSaveNew}
    />
  </div>
}
