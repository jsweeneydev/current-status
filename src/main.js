const React = require('react')
const { render } = require('react-dom')
const Main = require('./main.jsx')

const requests = require('./requests.js')

const { Provider, connect } = require('react-redux')
const redux = require('redux')

// Most peeps cannot edit on startup
let AllowEdit = !!window.localStorage.getItem('AllowEdit')

window.canEdit = () => AllowEdit
window.setEditable = () => {
  AllowEdit = true
  window.localStorage.setItem('AllowEdit', true)
}

const reducer = (state = {
  features: [],
  feature: {
    originalName: '',
    name: '',
    versions: [],
    selectedVersion: 0
  },
  originalText: '',
  currentText: ''
}, action) => {
  const { type, payload } = action
  switch (type) {
    case 'FEATURES_LOADED':
      return {
        ...state,
        features: payload
      }
    case 'FEATURE_LOADED':
      const selectedVersion = payload.versions.length - 1
      return {
        ...state,
        feature: {
          originalName: payload.name,
          name: payload.name,
          versions: payload.versions,
          selectedVersion
        },
        originalText: payload.versions[selectedVersion].text,
        currentText: payload.versions[selectedVersion].text
      }
    case 'NAME_CHANGED':
      return {
        ...state,
        feature: {
          ...state.feature,
          name: payload.name
        }
      }
    case 'MARKDOWN_CHANGED':
      return {
        ...state,
        currentText: payload.text
      }
    case 'BACK_PRESSED':
      return (() => {
        const index = Math.max(state.feature.selectedVersion - 1, 0)
        return {
          ...state,
          feature: {
            ...state.feature,
            selectedVersion: index
          }
          // currentText: state.feature.versions[index].text
        }
      })()
    case 'FORWARD_PRESSED':
      return (() => {
        const index = Math.max(
          Math.min(
            state.feature.selectedVersion + 1,
            state.feature.versions.length - 1
          ),
          0
        )
        return {
          ...state,
          feature: {
            ...state.feature,
            selectedVersion: index
          }
          // currentText: state.feature.versions[index].text
        }
      })()
  }
  return state
}

const store = redux.createStore(reducer)

const requestFeatures = () => (
  requests.getFeatures().then(features => {
    store.dispatch({
      type: 'FEATURES_LOADED',
      payload: features
    })
    return Promise.resolve(features)
  })
)
const requestFeature = name => (
  requests.getFeature(name).then(versions => {
    store.dispatch({
      type: 'FEATURE_LOADED',
      payload: { name, versions }
    })
  })
)

const requestSave = (options = { saveNew: false }) => {
  const { saveNew } = options
  const state = store.getState()
  const originalName = state.feature.originalName
  const name = state.feature.name
  return requests.saveFeature({
    originalName,
    name,
    text: state.currentText,
    saveNew
  })
    .then(versions => {
      requestFeatures()
        .then(() => {
          store.dispatch({
            type: 'FEATURE_LOADED',
            payload: { name, versions }
          })
        })
    })
}

const ConnectedMain = connect(
  // Map state to props
  state => {
    const featureObj = state.feature.versions[state.feature.selectedVersion]
    const isLastVersion = !!(
      featureObj &&
      state.feature.versions.length - 1 === state.feature.selectedVersion
    )
    const featureName = state.feature.name
    const featureText = (featureObj) ? featureObj.text : ''
    const date = (featureObj)
      ? new Date(featureObj.version.replace(/,/g, ':')).toString()
      : '(unkown)'

    const canSave = state.currentText !== state.originalText
    return {
      date,
      features: state.features,
      featureName: featureName,
      featureText: isLastVersion ? state.currentText : featureText,
      currentText: state.currentText,
      canEdit: !!AllowEdit,
      canSave: canSave,
      canCreateNew: canSave && !state.features.includes(featureName)
    }
  },
  // Map dispatch to props
  dispatch => {
    return {
      onFeatureSelected (featureName) {
        requestFeature(featureName)
      },
      onBackPressed () {
        dispatch({
          type: 'BACK_PRESSED',
          payload: {}
        })
      },
      onForwardPressed () {
        dispatch({
          type: 'FORWARD_PRESSED',
          payload: {}
        })
      },
      onNameChanged (name) {
        dispatch({
          type: 'NAME_CHANGED',
          payload: {
            name
          }
        })
      },
      onMarkdownChanged (text) {
        dispatch({
          type: 'MARKDOWN_CHANGED',
          payload: {
            text
          }
        })
      },
      onMarkdownSave () {
        requestSave()
      },
      onMarkdownSaveNew () {
        requestSave({ saveNew: true })
      }
    }
  }
)(Main)

render(
  <Provider store={store}>
    <ConnectedMain />
  </Provider>,
  document.querySelector('body')
)

// Load features automatically on startup
requestFeatures()
  .then(features => {
    if (features.length > 0) {
      requestFeature(features[0])
    }
  })
