const port = require('../scripts/config.js').port
const getUrl = addr =>
  `${
    window.location.origin.slice(0, window.location.origin.lastIndexOf(':'))
  }:${port}/${addr}`

module.exports = {
  getFeatures: () => {
    return window.fetch(getUrl('features'))
      .then(json => json.json())
  },
  getFeature: featureName => {
    return window.fetch(getUrl(`feature/${featureName}`))
      .then(json => json.json())
  },
  saveFeature: ({originalName, name, text, saveNew}) => {
    return window.fetch(getUrl(`feature/${originalName}`), {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({name, text, saveNew})
    })
      .then(json => json.json())
  }
}
