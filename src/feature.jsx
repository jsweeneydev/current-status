const React = require('react')
const Markdown = require('markdown').markdown

module.exports = ({
  featureName,
  featureText,
  currentText,
  canEdit,
  canSave,
  canCreateNew,
  onNameChanged,
  onMarkdownChanged,
  onMarkdownSave,
  onMarkdownSaveNew
}) => {
  return <div>
    <div dangerouslySetInnerHTML={{ __html: Markdown.toHTML(featureText) }} />
    {(() => {
      if (canEdit === true) {
        return <div>
          <hr />
          <input defaultValue={featureName} onChange={e => onNameChanged(e.srcElement.value)} /><br />
          <textarea defaultValue={currentText} cols='80' rows='20' onChange={e => onMarkdownChanged(e.srcElement.value)} />
          <br />
          <button disabled={!canSave} onClick={onMarkdownSave}>Save</button>
          <button disabled={!canCreateNew} onClick={onMarkdownSaveNew}>Save New</button>
        </div>
      }
    })()}
  </div>
}
