### Purpose

Relay to the customer and the team what the current understanding and status of a feature is.
This should allow for _actual_ Agile development; the markdown documents are transparent for all to see and are expected to update regularly. Plus, the history of modifications are kept so that old designs can be compared.

Anyone can look at features and their history, but only the team (specifically the Scrum Master or Team Lead) should be responsible for updating their understanding of a feature and its current status.



### Usage

To start the app, navigate to the folder it is in, install packages (`npm install`), and start (`node ./`). Then, navigate to http://localhost:8080/.



The top menu allows for switching to different features. The Back and Forward buttons navigate to older or newer versions of the feature description if there is any.



Anyone can view features and traverse to older and newer versions, but modifications have been baby-proofed. To create new features, open the browser console and run this command:

```js
window.setEditable()
```

Once the page is refreshed, options will appear at the bottom of the page that allow for renaming, modifying, and creating new features.



Features are saved in the "features" folder. The name of a feature is its own folder, and a version is a markdown file within these folders named with an ISO timestamp (where the colons are replaced with commas so that the file name is valid).



### Future Plans

The most important improvement would be an authentication method of some kind, so that it would be obvious who made a change to a feature. For now, trusting that customers or Product Owners won't butcher anything should suffice...