const express = require('express')
const bodyParser = require('body-parser')
const http = require('http')
const webpack = require('webpack')
const setupWebpackMiddleware = require('webpack-dev-middleware')
const fs = require('fs-extra')
const path = require('path')
const reload = require('reload')
const watch = require('watch')

const buildConfig = require('./config.js')
const generatePage = require('./generate-page.js')

const port = buildConfig.port

const includeReload = true

const app = express()
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
const reloadServer = reload(app, { verbose: true })
const webpackMiddleware = setupWebpackMiddleware(webpack({
  ...buildConfig.webpack,
  mode: 'development',
  output: {
    path: '/',
    filename: 'script.js',
    devtoolModuleFilenameTemplate: info => info.resourcePath
  }
}))
app.use(webpackMiddleware)
app.get('/', (req, res) => {
  res.status(200).send(generatePage({ includeReload }))
})
app.get('/style.css', (req, res) => {
  res.status(200).sendFile(path.resolve('./src/style.css'))
})

// - REST calls: ---------------------------------------------------------------
app.get('/features', (req, res) => {
  fs.readdir('./features')
    .then(dirs => {
      res.status(200).send(dirs.filter(dir => !dir.startsWith('.')))
    })
    .catch(error => {
      console.error(error)
      res.status(500).send(error)
    })
})

const getFeature = featureName => (
  fs.readdir(`./features/${featureName}`)
    .then(versions => (
      Promise.all(versions.filter(dir => !dir.startsWith('.')).map(version => (
        fs.readFile(`./features/${featureName}/${version}`, 'utf8')
          .then(text => ({
            version: version.substr(0, version.length - 3),
            text
          }))
      )))
    ))
)

app.get('/feature/:featureName', (req, res) => {
  const { featureName } = req.params
  getFeature(featureName)
    .then(versions => {
      res.status(200).send(versions)
    })
    .catch(error => {
      console.error(error)
      res.status(500).send(error)
    })
})

app.post('/feature/:featureName', (req, res) => {
  const originalName = req.params.featureName
  const { name, text, saveNew } = req.body
  const time = new Date().toISOString().replace(/:/g, ',')
  if (originalName !== name && !saveNew) {
    fs.moveSync(
      `./features/${originalName}`,
      `./features/${name}`
    )
  }

  fs.ensureDir(`./features/${name}`)
    .then(() => fs.writeFile(`./features/${name}/${time}.md`, text, 'utf8'))
    .then(() =>
      getFeature(name)
        .then(versions => {
          res.status(200).send(versions)
        })
    )
    .catch(error => {
      console.error(error)
      res.status(500).send(error)
    })
})
// -----------------------------------------------------------------------------

const server = http.createServer(app)
app.set('port', port)
server.listen(app.get('port'), () => {
  console.log(`Web server listening on port ${port}`)
})

if (includeReload) {
  // Reload the page if the script changes
  let lastScript = null
  setInterval(() => {
    const script = webpackMiddleware.fileSystem.data['script.js']
    if (lastScript !== script) {
      lastScript = script
      reloadServer.reload()
    }
  }, 50)
  // Reload the page if any non-script-related files change
  watch.createMonitor('./src/', monitor => {
    monitor.on('changed', f => {
      if (!(/.*.jsx?$/.test(f))) {
        reloadServer.reload()
      }
    })
  })
}
