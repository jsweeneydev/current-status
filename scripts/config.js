const path = require('path')
const babel = {
  presets: ['env'],
  plugins: [
    ['transform-react-jsx'],
    ['transform-object-rest-spread', {}]
  ]
}
const webpack = {
  entry: ['./src/main.js'],
  mode: 'production',
  module: {
    rules: [
      {
        use: [
          {
            loader: 'babel-loader',
            options: babel
          }
        ]
      }
    ]
  },
  output: {
    path: path.resolve('./index/'),
    filename: 'script.js'
  },
  resolve: {
    alias: {
      'react': 'preact-compat',
      'react-dom': 'preact-compat'
    }
  },
  devtool: 'source-map'
}
module.exports = {
  port: 8080,
  babel,
  webpack
}
